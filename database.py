# -*- coding: utf-8 -*-
__author__ = 'zax'

import sqlite3 as mdb
import csv
from unicode_writer import UnicodeWriter


# select min(time), team, answer from Logs where lvl=23 and (replace(answer, ' ', '')='149' and replace(answer, ' ', '')='') group by team;

class Database:

    def __init__(self):
        # connect to db
        self.con = mdb.connect('enstats.db')
        self.cur = self.con.cursor()

        self.cur.execute("SELECT SQLITE_VERSION()")
        data = self.cur.fetchone()
        print("Database version : %s " % data)

        # drop if needed
        self.cur.execute("DROP TABLE IF EXISTS Logs")

        # create table (if don't exist?)
        sql = """CREATE TABLE Logs (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                time DATE NOT NULL,
                lvl INTEGER NOT NULL,
                team TEXT NOT NULL,
                login TEXT NOT NULL,
                correct INTEGER NOT NULL,
                bonus INTEGER NOT NULL,
                answer TEXT NOT NULL
            )"""

        self.cur.execute(sql)

    def disconnect(self):
        self.cur.close()
        self.con.close()
        return

    def add_records(self, vals):
        # write to sqlite next
        try:
            self.cur.executemany("INSERT INTO Logs(time, lvl, team, login, correct, bonus, answer) VALUES(?, ?, ?, ?, ?, ?, ?)",
                                 vals)
            self.con.commit()
        except mdb.Error as e:
            print("cant add records to logs table", e.message)
            self.con.rollback()

        return 0
    
    def write_to_csv(self):
        output = UnicodeWriter(file('enstats.csv', 'w'))
        self.cur.execute('SELECT time, lvl, team, login, correct, bonus, answer from Logs ORDER BY time ASC;')
        output.writerow([col[0] for col in self.cur.description])
        filter(None, (output.writerow(row) for row in self.cur))

