# -*- coding: utf-8 -*-
__author__ = 'zax'

from bs4 import BeautifulSoup
import re
from reader_base import BaseReader
from database import Database
import time

from selenium import webdriver
import selenium.common.exceptions as se
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class EnReader(BaseReader):
    def __init__(self, url, game_id):
        BaseReader.__init__(self)
        
        if '://' in url:
            url = url[url.find('://')+3:]
        self.login_url = 'http://'+url+'/Login.aspx?return=%2fDefault.aspx'
        self.stats_url = 'http://'+url+'/Administration/Games/ActionMonitor.aspx?gid='

        self.db = Database()
        self.br = self.new_driver()
        
        self.nick = ""
        self.password = ""

        self.stats_url += game_id
    
    def close(self):
        self.br.close()
        self.db.disconnect()

    def specify_url(self, level, page):
        return self.stats_url + '&tab=0&page=%d&levels=%d' % (page, level)
    def specify_url_bonus(self, level, page):
        return self.stats_url + '&tab=1&page=%d&levels=%d' % (page, level)

    def login(self, login, password):
        print('logging in')
        
        self.nick = login
        self.password = password

        self.br.get(self.login_url)
        self.br.find_element_by_id('txtLogin').send_keys(login)
        self.br.find_element_by_id('txtPassword').send_keys(password)
        self.br.find_element_by_name('EnButton1').click()

        print('logged in')
    
    def get_soup_from_driver(self, m_link):
        response = ''
        wait = WebDriverWait(self.br, 10)
        while True:
            try:
                self.br.get(m_link)
                wait.until(ec.presence_of_element_located((By.ID, "MonitoringForm")))
                response = self.br.page_source.encode('utf-8')
                break
            except se.TimeoutException:
                print('Timeout exception', m_link)
                print("trying to log in")
                self.login(self.nick, self.password)
                continue

        return BeautifulSoup(response, "lxml")

    def read_level(self, level):
        page = 1
        while self.process_page(level, page):
            page += 1
            
    def read_level_bonus(self, level):
        page = 1
        while self.process_page_bonus(level, page):
            page += 1
    
    def read_levels(self):
        page = 1
        while self.process_page(0, page):
            page += 1
            time.sleep(0.01)
    
    def read_levels_bonus(self):
        page = 1
        while self.process_page_bonus(0, page):
            page += 1
            time.sleep(0.01)

    def process_page(self, level, page):
        print('reading %d' % page)

        soup = self.get_soup_from_driver(self.specify_url(level, page))
        table = soup.find('div', id='ctl03_divContent').find_all('table')[-1].find('tbody').find_all('tr')

        if len(table) == 4:
            return False

        vals = []
        for i, row in enumerate(table):
            if i < 4 or (i-4) % 4 != 0:
                continue

            vals.append([level, 1, 1, 1, 1, 0, 1])
            cols = row.find_all('td')
            
            # time
            uniString = unicode(cols[4].text)
            uniString = uniString.replace(u"\u00A0", " ")
            vals[-1][0] = unicode(self.format_date(uniString))
            
            # lvl
            vals[-1][1] = unicode(cols[0].text)

            # team, login
            team_log = cols[1].find_all('a')
            vals[-1][2] = unicode(team_log[0].text)
            vals[-1][3] = unicode(team_log[1].text)

            # correct
            vals[-1][4] = self.format_html2(cols[2].prettify('utf-8')).replace(' ', '')
            vals[-1][4] = 0 if vals[-1][4] == 'н' else 1

            # answer
            vals[-1][6] = unicode(cols[3].text.strip())
            #print('.'+vals[-1][6]+'.')
            
        self.db.add_records(vals)

        return True
    
    def process_page_bonus(self, level, page):
        print('reading bonus %d' % page)

        soup = self.get_soup_from_driver(self.specify_url_bonus(level, page))
        table = soup.find('div', id='ctl03_divContent').find_all('table')[-1].find('tbody').find_all('tr')

        if len(table) == 4:
            return False

        vals = []
        for i, row in enumerate(table):
            if i < 4 or (i-4) % 4 != 0:
                continue

            vals.append([level, 1, 1, 1, 1, 0, 1])
            cols = row.find_all('td')
            
            # time
            uniString = unicode(cols[5].text)
            uniString = uniString.replace(u"\u00A0", " ")
            vals[-1][0] = unicode(self.format_date(uniString))
            
            # lvl
            vals[-1][1] = unicode(cols[0].text)

            # team, login
            team_log = cols[2].find_all('a')
            vals[-1][2] = unicode(team_log[0].text)
            vals[-1][3] = unicode(team_log[1].text)

            # correct
            vals[-1][4] = self.format_html2(cols[3].prettify('utf-8')).replace(' ', '')
            vals[-1][4] = 0 if vals[-1][4] == 'н' else 1
            
            # bonus
            vals[-1][5] = unicode(cols[1].text)

            # answer
            vals[-1][6] = unicode(cols[4].text.strip())
            #print('.'+vals[-1][6]+'.')
            
        self.db.add_records(vals)

        return True

