__author__ = 'zax'

from en_reader import EnReader
import time
import argparse


'''This function parses and return arguments passed in'''
# Assign description to the help doc
parser = argparse.ArgumentParser(
    description='Script retrivies stats for game on en.cx')
# Add arguments
parser.add_argument(
    '-l', '--login', type=str, help='Login', required=True)
parser.add_argument(
    '-p', '--password', type=str, help='Password', required=True)
parser.add_argument(
    '-u', '--url', type=str, help='Domain url (e.g. moscow.en.cx)', required=True)
parser.add_argument(
    '-g', '--gid', type=str, help='Game id (e.g. 12345)', required=True)
parser.add_argument(
    '-c', '--lvlnum', type=int, help='Level number to fetch (optional)')
# Array for all arguments passed to script
args = parser.parse_args()
# Assign args to variables
login = args.login
password = args.password
url = args.url
gid = args.gid
lnum = args.lvlnum

reader = EnReader(url, gid)
reader.login(login, password)

if lnum is not None:
    reader.read_level(lnum)
    time.sleep(0.1)
    reader.read_level_bonus(lnum)
else:
    reader.read_levels()
    time.sleep(0.1)
    reader.read_levels_bonus()

reader.db.write_to_csv()

reader.close()

