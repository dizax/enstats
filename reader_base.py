# -*- coding: utf-8 -*-
__author__ = 'zax'

from bs4 import BeautifulSoup
import re
from selenium import webdriver
import selenium.common.exceptions as se
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from datetime import datetime


class BaseReader:

    def __init__(self):
        self.br = self.new_driver()
        #self.wait = WebDriverWait(self.br, 10)

    def close(self):
        self.br.quit()

    @staticmethod
    def new_driver():
        #cookie_file_path = 'cookie.txt'
        #args = ['--cookies-file={}'.format(cookie_file_path)]
        args = ['--load-images=no']

        user_agent = (
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 " +
            "(KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36"
        )
        d_cap = dict(DesiredCapabilities.PHANTOMJS)
        d_cap["phantomjs.page.settings.userAgent"] = user_agent
        
        driver = webdriver.PhantomJS(service_args=args, desired_capabilities=d_cap)

        return driver

    def get_soup_from_driver(self, m_link):
        response = ''
        wait = WebDriverWait(self.br, 10)
        while True:
            try:
                self.br.get(m_link)
                wait.until(ec.presence_of_element_located((By.ID, "MonitoringForm")))
                response = self.br.page_source.encode('utf-8')
                break
            except se.TimeoutException:
                print('Timeout exception', m_link)
                print("trying to log in")
                continue

        #f = file(m_link[40:].replace('/', '_') + '.html', 'w')
        #f.write(response)
        #f.close()
        return BeautifulSoup(response, "lxml")

    def get_soup_from_driver_js(self, m_link, m_js):
        response = ''
        wait = WebDriverWait(self.br, 10)
        while True:
            try:
                self.br.get(m_link)
                self.br.execute_script(m_js)
                wait.until(ec.presence_of_element_located((By.ID, "MonitoringForm")))
                response = self.br.page_source.encode('utf-8')
                break
            except se.TimeoutException:
                print('Timeout exception js', m_link)
                continue

        return BeautifulSoup(response, "lxml")

    @staticmethod
    def format_html(m_text):
        m_text = re.sub('<[^>]*>', '', m_text)
        m_text = re.sub(' +\s+', '\n', m_text)
        return re.sub('\n+', '\n', m_text)

    @staticmethod
    def format_html2(m_text):
        m_text = re.sub('<[^>]*>', '', m_text)
        m_text = re.sub(' +\s+', '\n', m_text)
        m_text = re.sub('\n+', '\n', m_text)
        return m_text.replace('\n', ' ')

    @staticmethod
    def format_date(m_date):
        return datetime.strptime(m_date, '%d.%m.%Y %H:%M:%S.%f').strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def find_between(s, first, last):
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""

    @staticmethod
    def find_before(s1, s2):
        try:
            return s1[:s1.index(s2)]
        except ValueError:
            return ""

    @staticmethod
    def find_after(s1, s2):
        try:
            return s1[s1.index(s2) + len(s2):]
        except ValueError:
            return ""
