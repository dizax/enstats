####Description:
Script retrieves stats for game from en.cx.  
Requires account with access to monitoring given game stats.

####Requirements:
* python 2.7;  
* bs4 (BeautifulSoup4);  
* Selenium;  
* csv;  
* sqlite3.

####Usage:
To start fetching stats from given game:  
```
main.py [-h] -l LOGIN -p PASSWORD -u DOMAIN -g GID [-c LEVELNUM]
```
To convert from sqlite db to csv, use this command for sqlite3:  
```
>sqlite3 enstats.db
sqlite> .header on
sqlite> .mode csv
sqlite> .output enstats.csv
sqlite> SELECT * FROM Logs ORDER BY time ASC;
sqlite> .quit
```

